﻿using INPS.SocketIO.Client.CommonPortable;
using INPS.SocketIO.Client.CommonPortable.Message;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Windows.Networking.Sockets;
using Windows.Storage.Streams;

namespace INPS.SocketIO.Client.RT81
{
    public class SocketIO : SocketIOBase, IDisposable, ISocketClient
    {
        #region Instance variables

        MessageWebSocket websocket;
        DataWriter messageWriter;

        Timer HeartbeatTimer;
        Timer TimeoutTimer;

        #endregion

        #region Events
        /// <summary>
        /// Callback executes when a socket fails to connect to the target endpoint.
        /// The error message is passed in.
        /// </summary>
        public override event Action<string> SocketFailedToConnect = delegate { };
        #endregion

        #region Constructors

        /// <summary>
        /// Initialize new instance of <see cref="SocketIO4NetLite.SocketIO"/> class on localhost, port 3000
        /// </summary>
        public SocketIO() : this("127.0.0.1", 3000) { }

        public SocketIO(string host, int port = 80, bool secure = false)
        {
            this.Host = host;
            this.Port = port;
            this.Secure = secure;
            this.SocketConnectionType = ConnectionType.LongPolling;
            this.Status = ConnectionStatus.Closed;

            JsonConvert.DefaultSettings = () =>
            {
                return new JsonSerializerSettings()
                {
                    StringEscapeHandling = StringEscapeHandling.EscapeNonAscii
                };
            };

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Send a string message to the server. This allows for manual messages to be sent.
        /// ToDo: need to properly implement endpoint and possibly messageID with the new v1.X protocol.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="enpoint"></param>
        /// <param name="messageID"></param>
        public override async void Send(string message)
        {
            if (Status == ConnectionStatus.Connected)
            {
                if (this.SocketConnectionType == ConnectionType.WebSocket)
                {
                    messageWriter.WriteString(message);
                    // Send the data as one complete message.
                    await messageWriter.StoreAsync();
                    Debug.WriteLine("Sent message: {0}", message);
                }
                else if (this.SocketConnectionType == ConnectionType.LongPolling)
                {
                    this.SendHttpPost(message);
                    Debug.WriteLine("Sent message: {0}", message);
                }
            }
        }

        /// <summary>
        /// Send a pre-defined message up the socket connection.
        /// </summary>
        /// <param name="message"></param>
        public override void Send(IMessage message)
        {
            this.Send(message.Encoded);
        }

        #endregion

        #region Private / Protected Methods

        /// <summary>
        /// Attempt to open the websocket connection.
        /// Returns true only the connection successfully opens. 
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        protected override async Task<bool> AttemptOpenWebSocket(HttpClient client)
        {
            try
            {
                string scheme = Secure ? "wss" : "ws";
                string websocketUri = string.Format("{0}://{1}:{2}/{3}/?transport=websocket&sid={4}", scheme, Host, Port, SocketIOUrlString, sessionID);

                websocket = new MessageWebSocket();
                websocket.Control.MessageType = SocketMessageType.Utf8;
                AddCallbacksToWebsocket(ref websocket);
                await websocket.ConnectAsync(new Uri(websocketUri));
                this.messageWriter = new DataWriter(websocket.OutputStream);

                OnSocketOpened(null, null);

                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(string.Format("Exception thrown while trying to open the websocket. Message: {0}", ex.Message));
                SocketFailedToConnect(ex.Message);
                return false;
            }
        }

        void AddCallbacksToWebsocket(ref MessageWebSocket socket)
        {
            socket.Closed += MessageWebsocketClosed;
            socket.MessageReceived += OnSocketMessageReceived;
        }

        void MessageWebsocketClosed(IWebSocket sender, WebSocketClosedEventArgs args)
        {
            SocketClosed(sender, null);
        }     

        protected override void MessageReceived(string message)
        {
            HandleMessageReceived(message);
        }

        void OnSocketMessageReceived(MessageWebSocket sender, MessageWebSocketMessageReceivedEventArgs args)
        {      
            string message = ParseReceivedArgs(args);
            HandleMessageReceived(message);
        }

        string ParseReceivedArgs(MessageWebSocketMessageReceivedEventArgs args)
        {
            using (DataReader dr = args.GetDataReader())
            {
                dr.UnicodeEncoding = Windows.Storage.Streams.UnicodeEncoding.Utf8;
                return dr.ReadString(dr.UnconsumedBufferLength);
            }
        }

        /// <summary>
        /// Send the message to disconnect from the specified namespace/endpoint.
        /// If the endpoint is not given, this will call for the socket connection to disconnect. 
        /// </summary>
        /// <param name="o"></param>
        /// <param name="socketNamespace"></param>
        protected override void SendDisconnectMessage(object o, string socketNamespace = "")
        {
            if (Status == ConnectionStatus.Connected)
            {
                Debug.WriteLine("Sending Disconnect Message.");
                this.Send(new Disconnect(socketNamespace));

                // If the Endpoint is blank, disconnect the socket connection.
                if (string.IsNullOrEmpty(socketNamespace))
                {
                    if (SocketConnectionType == ConnectionType.WebSocket)
                        websocket.Close(1000, "Disconnect sent by client.");

                    HeartbeatTimer.Change(0, 0);
                    HeartbeatTimer.Dispose();
                    TimeoutTimer.Change(0, 0);
                    TimeoutTimer.Dispose();
                    Status = ConnectionStatus.Closed;

                    OnSessionDisconnected("Disconnect message sent from client.");
                }
            }
            else
            {
                Debug.WriteLine("Error: websocket must be 'Connected' in order to disconnect");
            }
        }


        #endregion
    }
}
