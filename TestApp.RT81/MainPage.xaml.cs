﻿using INPS.SocketIO.Client.CommonPortable.Message;
using INPS.SocketIO.Client.RT81;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace TestApp.RT81
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        SocketIO _client = null;
        
        public MainPage()
        {
            this.InitializeComponent();
        }

        private async void btnTest_Click(object sender, RoutedEventArgs e)
        {
            _client = new SocketIO("www.inpsprototypes.co.uk", 3111, false);
            _client.SessionEstablished += this.OnSocketConnected;
            _client.SessionDisconnected += this.OnSocketDisconnect;
            _client.SocketFailedToConnect += this.OnSocketConnectFailed;
            _client.On("notification", this.OnNotificationEvent);

            await _client.ConnectAsync();
        }

        private void OnNotificationEvent(JToken obj)
        {
            System.Diagnostics.Debug.WriteLine("Notification Received! :).");
        }

        private void OnSocketConnectFailed(string obj)
        {
            System.Diagnostics.Debug.WriteLine("Connection Failed.");
        }

        private void OnSocketDisconnect(string reason)
        {
            System.Diagnostics.Debug.WriteLine("Connection Closed.");
        }

        private void OnSocketConnected(INPS.SocketIO.Client.CommonPortable.Message.IMessage arg1)
        {
            System.Diagnostics.Debug.WriteLine("Connection Opened.");
            _client.Send(new EventMessage("register", "{\"practices\":[{\"id\":\"CLOUD_PRACTICE_1\"}]}"));
        }
    }
}
