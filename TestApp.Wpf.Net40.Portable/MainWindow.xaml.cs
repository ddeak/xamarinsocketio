﻿using INPS.SocketIO.Client.CommonPortable.Message;
using INPS.SocketIO.Client.Net40.Portable;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TestApp.Wpf.Net40.Portable
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        SocketIO _client = null;

        public MainWindow()
        {
            InitializeComponent();
        }

        private async void btnTest_Click(object sender, RoutedEventArgs e)
        {
            _client = new SocketIO("www.inpsprototypes.co.uk", 3111, false);
            _client.SessionEstablished += this.OnSocketConnected;
            _client.SessionDisconnected += this.OnSocketDisconnect;
            _client.SocketFailedToConnect += this.OnSocketConnectFailed;
            _client.On("notification", this.OnNotificationEvent);

            await _client.ConnectAsync();
        }

        private void OnNotificationEvent(JToken obj)
        {
            Console.WriteLine("Notification Received! :)");
        }

        private void OnSocketConnectFailed(string obj)
        {
            Console.WriteLine("Connection Failed.");
        }

        private void OnSocketDisconnect(string reason)
        {
            System.Diagnostics.Debug.WriteLine("Connection Closed.");
        }

        private void OnSocketConnected(IMessage arg1)
        {
            Console.WriteLine("Connection Opened.");
            _client.Send(new EventMessage("register", "{\"practices\":[{\"id\":\"CLOUD_PRACTICE_1\"}]}"));
        }
    }
}
