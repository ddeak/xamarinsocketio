﻿using SocketIO.Client.CommonPortable;
using SocketIO.Client.CommonPortable.Message;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebSocket4Net;

namespace SocketIO.Client.Net40.Portable
{
    public class SocketIO : SocketIOBase, IDisposable, ISocketClient
    {
        #region Instance variables

        WebSocket websocket;

        #endregion

        #region Events
        /// <summary>
        /// Callback executes when a socket fails to connect to the target endpoint.
        /// The error message is passed in.
        /// </summary>
        public override event Action<string> SocketFailedToConnect = delegate { };

        #endregion

        #region Constructors

        /// <summary>
        /// Initialize new instance of <see cref="SocketIO4NetLite.SocketIO"/> class on localhost, port 3000
        /// </summary>
        public SocketIO() : this("127.0.0.1", 3000) { }

        public SocketIO(string host, int port = 80, bool secure = false)
        {
            this.Host = host;
            this.Port = port;
            this.Secure = secure;
            this.SocketConnectionType = ConnectionType.LongPolling;
            this.Status = ConnectionStatus.Closed;

            JsonConvert.DefaultSettings = () =>
            {
                return new JsonSerializerSettings()
                {
                    StringEscapeHandling = StringEscapeHandling.EscapeNonAscii
                };
            };
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Send a string message to the server. This method allows messages to be manually contructed and sent to the server.
        /// ToDo: For production, this method should be made private/protected.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="enpoint"></param>
        /// <param name="messageID"></param>
        public override async void Send(string message)
        {
            if (this.SocketConnectionType == ConnectionType.WebSocket)
            {
                if (Status == ConnectionStatus.Connected)
                    websocket.Send(string.Format("{0}", message));
            }
            else if (this.SocketConnectionType == ConnectionType.LongPolling)
            {
                this.SendHttpPost(message);
            }
        }

        /// <summary>
        /// Send a pre-defined message up the socket connection.
        /// </summary>
        /// <param name="message"></param>
        public override void Send(IMessage message)
        {
            this.Send(message.Encoded);
        }

        #endregion

        #region Private / Protected Methods

        /// <summary>
        /// Attempt to open the websocket connection.
        /// Returns true only the connection successfully opens. 
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        protected async override Task<bool> AttemptOpenWebSocket(HttpClient client)
        {
            try
            {
                string scheme = Secure ? "wss" : "ws";
                string websocketUri = string.Format("{0}://{1}:{2}/{3}/?transport=websocket&sid={4}", scheme, Host, Port, SocketIOUrlString, sessionID);

                websocket = new WebSocket(websocketUri);
                websocket.Opened += OnSocketOpened;
                websocket.Closed += SocketClosed;
                websocket.DataReceived += OnSocketDataReceived;
                websocket.MessageReceived += OnSocketMessageReceived;
                websocket.Error += OnSocketError;

                websocket.Open();

                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(string.Format("Exception thrown while trying to open the websocket. Message: {0}", ex.Message));
                this.SocketFailedToConnect(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Send the message to disconnect from the specified endpoint.
        /// If the endpoint is NOT provided, this will call for the session to be terminated (and the websocket to close).
        /// </summary>
        /// <param name="o"></param>
        /// <param name="endpoint"></param>
        protected override void SendDisconnectMessage(object o, string socketNamespace = "")
        {
            if (Status == ConnectionStatus.Connected)
            {
                Debug.WriteLine("Sending Disconnect Message.");
                this.Send(new Disconnect(socketNamespace));

                // If the Endpoint is blank, disconnect the socket connection.
                if (string.IsNullOrEmpty(socketNamespace))
                {
                    if (SocketConnectionType == ConnectionType.WebSocket)
                        websocket.Close();

                    HeartbeatTimer.Change(0, 0);
                    HeartbeatTimer.Dispose();
                    TimeoutTimer.Change(0, 0);
                    TimeoutTimer.Dispose();
                    Status = ConnectionStatus.Closed;

                    OnSessionDisconnected("Disconnect message sent from client.");
                }
            }
            else
            {
                Debug.WriteLine("Error: websocket must be 'Connected' in order to disconnect");
            }
        }

        protected override void MessageReceived(string message)
        {
            OnSocketMessageReceived(null, new MessageReceivedEventArgs(message));
        }

        /// <summary>
        /// Parse and handle messages received from the server.
        /// example open message: 40
        /// example event message: 42["eventName",{data}]
        /// </summary>
        /// <param name="o"></param>
        /// <param name="e"></param>
        void OnSocketMessageReceived(object o, MessageReceivedEventArgs e)
        {
            // Reset Timeout when a message is received. 
            TimeoutTimer.Change(TimeoutTime, Timeout.Infinite);

            Debug.WriteLine("Received Message: {0}", e.Message);
            int engineType;

            try
            {
                engineType = int.Parse(e.Message[0].ToString());
            }
            catch (Exception ex)
            {
                Debug.WriteLine(string.Format("MessageReceived-Error: Failed to convert first value (Engine) of message to integer - {0}", ex.Message));
                return;
            }

            switch (engineType)
            {
                case (int)EngineIOMessageType.Connect:
                    // Connect
                    Debug.WriteLine("Connect Received.");
                    break;
                case (int)EngineIOMessageType.Close:
                    // Close
                    Debug.WriteLine("Close Received.");
                    break;
                case (int)EngineIOMessageType.Ping:
                    // Ping
                    Debug.WriteLine("Ping Received.");
                    SendPong();
                    break;
                case (int)EngineIOMessageType.Pong:
                    // Pong
                    Debug.WriteLine("Pong Received.");
                    break;
                case (int)EngineIOMessageType.Message:
                    // Message
                    Debug.WriteLine("Message Received.");
                    HandleMessage(e.Message);
                    break;
                case (int)EngineIOMessageType.Upgrade:
                    // Upgrade
                    Debug.WriteLine("Upgrade Received.");
                    break;
                case (int)EngineIOMessageType.Noop:
                    // Noop
                    Debug.WriteLine("Noop Received.");
                    break;
                default:
                    Debug.WriteLine("Error: Invalid message received.");
                    Debug.WriteLine("Message: {0}", e.Message);
                    break;
            }
        }

        /// <summary>
        /// Called when a websocket error occurs.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnSocketError(object sender, SuperSocket.ClientEngine.ErrorEventArgs e)
        {
            Debug.WriteLine(string.Format("Websocket Error: {0}", e.Exception.Message));
        }

        /// <summary>
        /// Called when Data is sent from the server to the client.
        /// </summary>
        /// <param name="o"></param>
        /// <param name="e"></param>
        void OnSocketDataReceived(object o, WebSocket4Net.DataReceivedEventArgs e)
        {
            Debug.WriteLine("Socket Received data");
            Debug.WriteLine(e.Data.ToString());
        }

        #endregion

    }
}
