﻿//------------------------------------------------------------------
//
// Created by: Daniel Deak
// 
//------------------------------------------------------------------

namespace SocketIO.Client.CommonPortable
{
    #region Using Statements

    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;

    using ISocketIO.Client.CommonPortable.Message;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    #endregion

    /// <summary>
    /// Abstract base class for Socket IO Clients.
    /// Implements ISocketClient.
    /// </summary>
    public abstract class SocketIOBase : ISocketClient
    {
        #region Private Members

        /// <summary>
        /// This forms part of the URL string - used in Socket IO Handshake, Http Post and Http Get operations.
        /// </summary>
        protected const string SocketIOUrlString = "socket.io";

        /// <summary>
        /// This forms part of the URL string - used in Socket IO Handshake, Http Post and Http Get operations.
        /// </summary>
        protected const string SessionQuery = "?transport=polling&b64=true";

        /// <summary>
        /// The message acknowledgement identifier.
        /// </summary>
        protected int ackId = 0;

        /// <summary>
        /// The epoch start time (01/01/1970 00:00:00).
        /// </summary>
        protected DateTime epochStartCompareTime = new DateTime(1970, 1, 1);

        /// <summary>
        /// The event handler functions stored by event name.
        /// </summary>
        protected Dictionary<string, List<Action<JToken>>> eventHandlers = new Dictionary<string, List<Action<JToken>>>();

        /// <summary>
        /// The period between sending 'ping' / heartbeat messages.
        /// This is set from server provided values during the handshake process. 
        /// </summary>
        protected int heartbeatPingInterval;

        /// <summary>
        /// The polling in progress indicator.
        /// </summary>
        protected bool pollingInProgress = false;

        /// <summary>
        /// The session identifier for the Socket IO connection.
        /// This is set from server provided values during the handshake process. 
        /// </summary>
        protected string sessionID;

        /// <summary>
        /// The timeout value for the ping requests.
        /// This is set from server provided values during the handshake process. 
        /// </summary>
        protected int pingTimeoutDuration;

        #endregion

        #region Constructors

        #endregion

        #region Events

        /// <summary>
        /// ToDo:
        /// This callback should execute when the server is no longer aware of our session.
        /// I think this event should be used to trigger a reconnect method. 
        /// Currently it is triggered if a GET request returns unsuccessfully.
        /// </summary>
        public event Action<string> SessionDisconnected = delegate { };

        /// <summary>
        /// Callback executes when a connection the handshake is successful with the Socket.IO server.
        /// </summary>
        public event Action<IMessage> SessionEstablished = delegate { };

        /// <summary>
        /// Callback executes if the handshake with the server fails. 
        /// </summary>
        public virtual event Action<string> SessionFailedToEstablish = delegate { };

        /// <summary>
        /// Callback executes when a socket fails to connect to the target endpoint.
        /// The error message is passed in.
        /// </summary>
        public virtual event Action<string> SocketFailedToConnect = delegate { };

        /// <summary>
        /// Callback executes when the connection times out. 
        /// </summary>
        public event Action TimedOut = delegate { };

        /// <summary>
        /// Callback executes when a web socket successfully opens.
        /// </summary>
        public event Action WebsocketOpened = delegate { };

        /// <summary>
        /// Callback executes when a web socket is closed.
        /// ToDo: This should take a string which provides details as to why the socket was closed. 
        /// Currently this is a problem due to the RT client having a different EventArgs parameter. 
        /// </summary>
        public event Action WebsocketClosed = delegate { };

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the type of the socket connection.
        /// </summary>
        /// <value>
        /// The type of the socket connection.
        /// </value>
        public ConnectionType SocketConnectionType { get; set; }

        /// <summary>
        /// Gets or sets the status of the connection.
        /// </summary>
        /// <value>
        /// The status of the connection.
        /// </value>
        public ConnectionStatus Status { get; set; }

        /// <summary>
        /// Gets or sets the heartbeat timer.
        /// </summary>
        /// <value>
        /// The heartbeat timer.
        /// </value>
        protected virtual Timer HeartbeatTimer { get; set; }

        /// <summary>
        /// Gets or sets the host.
        /// </summary>
        /// <value>
        /// The host.
        /// </value>
        protected string Host { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance uses a secure communication protocol.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance uses a secure communication protocol; otherwise, <c>false</c>.
        /// </value>
        protected bool IsSecure { get; set; }

        /// <summary>
        /// Gets the message acknowledgement identifier.
        /// Increments on each read to ensure unique message id.
        /// </summary>
        /// <value>
        /// The message acknowledgement identifier.
        /// </value>
        protected int MessageAckId
        {
            get
            {
                return this.ackId++;
            }
        }

        /// <summary>
        /// Gets the message timestamp.
        /// </summary>
        /// <value>
        /// The message timestamp.
        /// </value>
        protected int MessageTimestamp
        {
            get
            {
                return System.Convert.ToInt32(DateTime.Now.Subtract(this.epochStartCompareTime).TotalSeconds);
            }
        }

        /// <summary>
        /// Gets or sets the port.
        /// </summary>
        /// <value>
        /// The port.
        /// </value>
        protected int Port { get; set; }

        /// <summary>
        /// Gets or sets the timeout timer.
        /// </summary>
        /// <value>
        /// The timeout timer.
        /// </value>
        protected virtual Timer TimeoutTimer { get; set; }

        #endregion

        #region Public Methods

        /// <summary>
        /// Connects to http(s)://host:port/ asynchronously.
        /// </summary>
        /// <returns>Task objects provides status information to caller.</returns>
        public async Task ConnectAsync()
        {
            await this.ConnectAsync(string.Empty);
        }

        /// <summary>
        /// Connects to http(s)://host:port/ asynchronously.
        /// Currently takes a string that should represent the extra query parameters defined by the user. 
        /// ToDo: Either implement the method to use the query, or remove it.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <returns>Task objects provides status information to caller.</returns>
        public async Task ConnectAsync(string query)
        {
            // If already connected or connecting, do not attempt another connection. 
            if (this.Status == ConnectionStatus.Connected)
            {
                return;
            }

            if (this.Status == ConnectionStatus.Connecting)
            {
                return;
            }

            this.Status = ConnectionStatus.Connecting;

            using (HttpClient client = new HttpClient())
            {
                // Perform handshake. Return closed if it fails.

                bool handShakeSuccessful = await this.PerformHandShake(client);
                if (!handShakeSuccessful)
                {
                    this.Status = ConnectionStatus.Closed;
                    return;
                }

                this.HeartbeatTimer = new Timer(_ =>
                {
                    this.SendPing();
                },
                null, this.heartbeatPingInterval, this.heartbeatPingInterval); // latency?

                this.TimeoutTimer = new Timer(_ =>
                {
                    this.Disconnect();
                    this.TimedOut();
                }, null, this.pingTimeoutDuration, Timeout.Infinite);

                this.Status = ConnectionStatus.Connected;

                // Start with LongPolling
                // Attempt to receive a GET request from the server.
                // If this is successful, the session was established successfully.
                this.pollingInProgress = await this.SendHttpGet();
                if (this.pollingInProgress)
                {
                    if (this.SessionEstablished != null)
                    {
                        this.SessionEstablished(null);
                    }
                }
                else
                {
                    this.Status = ConnectionStatus.Closed;
                    return;
                }

                await TaskEx.Delay(1000);

                // Do not await as may not happen.
                this.AttemptOpenWebSocket(client);
            }
        }

        /// <summary>
        /// Connects to a specified namespace (endpoint).
        /// </summary>
        /// <param name="socketNamespace">The socket namespace.</param>
        public void ConnectToNameSpace(string socketNamespace)
        {
            if (!string.IsNullOrEmpty(socketNamespace))
            {
                if (this.Status == ConnectionStatus.Connected)
                {
                    this.Send(new Connect(socketNamespace));
                }
            }
        }

        /// <summary>
        /// Disconnect from the specified endpoint.
        /// If no endpoint is provided, this will disconnect the socket connection.
        /// </summary>
        /// <param name="socketNamespace">The socket namespace.</param>
        public void Disconnect(string socketNamespace = "")
        {
            if ((this.Status == ConnectionStatus.Connected) || (this.Status == ConnectionStatus.Connecting))
            {
                this.SendDisconnectMessage(null, socketNamespace);
            }
        }

        /// <summary>
        /// Equivalent to socket.on("name", function (data) { }) in JavaScript.
        /// The callback is executed when the server emits an event that matches <paramref name="name">Name</paramref>
        /// </summary>
        /// <param name="name">The name of the event.</param>
        /// <param name="callback">The delegate function to call when an event of the given name occurs.</param>
        public void On(string name, Action<JToken> callback)
        {
            if (!string.IsNullOrEmpty(name))
            {
                // check if the disctionary has a reference for this name already.
                // If it does, add the callback to be run when it is called.
                // else, create a new action with the callback.
                if (this.eventHandlers.ContainsKey(name))
                {
                    this.eventHandlers[name].Add(callback);
                }
                else
                {
                    this.eventHandlers[name] = new List<Action<JToken>>() { callback };
                }
            }
        }

        /// <summary>
        /// Send a string message to the server. This allows for manual messages to be sent.
        /// </summary>
        /// <param name="message">The message to send.</param>
        public virtual async void Send(string message)
        {
            Debug.WriteLine("Error: Send method should be overriden in derived class.");
        }

        /// <summary>
        /// Send a pre-defined message up the socket connection.
        /// </summary>
        /// <param name="message">The message.</param>
        public abstract void Send(IMessage message);

        /// <summary>
        /// Send an 'event' message up the socket if it is connected. 
        /// </summary>
        /// <param name="name">The event name.</param>
        /// <param name="data">The data to send.</param>
        /// <param name="socketNamespace">The socket namespace.</param>
        public void Emit(string name, IEnumerable data, string socketNamespace = "")
        {
            if (this.Status == ConnectionStatus.Connected)
            {
                if (!string.IsNullOrEmpty(name))
                {
                    this.Send(new EventMessage(name, data, socketNamespace));
                }
                else
                {
                    Debug.WriteLine("Error: Tried to emit null or empty name.");
                }
            }
            else
            {
                Debug.WriteLine("Error: websocket must be 'Connected' in order to emit");
            }
        }

        /// <summary>
        /// Sends the acknowledgment.
        /// </summary>
        public void SendAcknowledgment() 
        { 
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        public void Dispose()
        {
            this.Disconnect();
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Connect to the socket.io service on the server and retrieve the connection details: SessionID, Upgrades, Ping and Timeout.
        /// </summary>
        /// <param name="client">An HttpClient to use to perform the operation.</param>
        /// <returns>Returns <c>true</c> if the handshake is successful; otherwise, <c>false</c>.</returns>
        protected async Task<bool> PerformHandShake(HttpClient client)
        {
            try
            {
                // Example Url: "http(s)://localhost:3000/socket.io/?transport=polling&b64=true&t=1412667249809-0" 
                string scheme = this.IsSecure ? "https" : "http";
                string handShakeUri = string.Format("{0}://{1}:{2}/{3}/{4}&t={5}-{6}", scheme, this.Host, this.Port, SocketIOUrlString, SessionQuery, this.MessageTimestamp, this.MessageAckId);
                // Obtain socket.io conenctiong details.
                string responseBody = await client.GetStringAsync(handShakeUri);

                if (!string.IsNullOrEmpty(responseBody))
                {
                    // example response: 97:0{"sid":"<value>","upgrades":["websocket"],"pingInterval":25000,"pingTimeout":60000}
                    // ToDo: There is likely a better method than using substring..
                    string jsonstring = responseBody.Substring(responseBody.IndexOf('{'));
                    SocketConnectionString connectionValues = JsonConvert.DeserializeObject<SocketConnectionString>(jsonstring);

                    this.sessionID = connectionValues.SId;
                    this.heartbeatPingInterval = connectionValues.PingInterval;
                    this.pingTimeoutDuration = connectionValues.PingTimeout;
                    // ToDo: add the 'upgrade' options here.
                }

                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(string.Format("Exception thrown while trying to handshake. Message: {0}", ex.Message));
                this.SessionFailedToEstablish(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// In a derived class, should implement a method to upgrade the connection to a web socket. 
        /// Frameworks support different methods of using web sockets. 
        /// </summary>
        /// <param name="client">An HttpClient to use to perform the operation.</param>
        /// <returns>Returns <c>true</c> if the web socket is opened; otherwise, <c>false</c>.</returns>
        protected abstract Task<bool> AttemptOpenWebSocket(HttpClient client);

        /// <summary>
        /// Attempts to reconnect to the server.
        /// </summary>
        protected async void AttemptReconnect()
        {
            int retryCount = 0;
            int maxRetryAttemptsAllowed = 3;
            int reconnectInterval = 5000; // milisecond between attempts.

            Timer reconnectTimer = new Timer(async _ =>
            {
                if (retryCount > maxRetryAttemptsAllowed)
                {
                    Debug.WriteLine("Reconnecting to server has failed. Closing session.");
                    this.Disconnect();
                    return;
                }

                Debug.WriteLine(string.Format("Reconnect attempt {1}", retryCount));

                retryCount++;
                await ConnectAsync();

                if (Status == ConnectionStatus.Connected)
                {
                    return;
                }
            }, null, reconnectInterval, reconnectInterval);
        }

        /// <summary>
        /// This method runs when a web socket successfully opens.
        /// </summary>
        /// <param name="o">The sender?</param>
        /// <param name="e">The EventArgs.</param>
        protected void OnSocketOpened(object o, EventArgs e)
        {
            Debug.WriteLine("WebSocket Opened.");

            // Probe the websocket to see if messages can be successfully sent. 
            this.SocketConnectionType = ConnectionType.WebSocket;
            this.Send("2probe");

            // Revert the status to Longpolling incase the socket is unable to transfer messages. 
            this.SocketConnectionType = ConnectionType.LongPolling;
        }

        /// <summary>
        /// Upgrades to a web socket connection.
        /// </summary>
        protected void UpgradeToWebsocket()
        {
            // Update the ConnectionType to stop the long polling.          
            this.SocketConnectionType = ConnectionType.WebSocket;

            // Send the upgrade signal up the websocket.
            this.Send(new Upgrade());
            this.pollingInProgress = false;

            if (this.WebsocketOpened != null)
            {
                this.WebsocketOpened();
            }
        }

        /// <summary>
        /// This method executes when a web socket's status is changed to 'closed'.
        /// When this occurs, this will attempt to regress to LongPolling if it is not already running.  
        /// </summary>
        /// <param name="o">The sender?</param>
        /// <param name="e">The Event Args.</param>
        protected void SocketClosed(object o, EventArgs e)
        {
            Debug.WriteLine("WebSocket Closed.");
            this.SocketConnectionType = ConnectionType.LongPolling;

            if (this.WebsocketClosed != null)
            {
                this.WebsocketClosed();
            }

            this.StartPolling();
        }

        /// <summary>
        /// Called when a session is disconnected.
        /// </summary>
        /// <param name="reason">The reason.</param>
        protected void OnSessionDisconnected(string reason = "")
        {
            if (this.SessionDisconnected != null)
            {
                this.SessionDisconnected(reason);
            }
        }

        /// <summary>
        /// Manages the Engine.io part of the message - which is the first character (number) in the message. 
        /// </summary>
        /// <param name="message">The message.</param>
        protected void HandleMessageReceived(string message)
        {
            // Reset Timeout when a message is received. 
            this.TimeoutTimer.Change(this.pingTimeoutDuration, Timeout.Infinite);

            Debug.WriteLine("Received Message: {0}", message);
            int engineType;
            // example open message: 40
            // example event message: 42["eventName",{data}]
            try
            {
                engineType = int.Parse(message[0].ToString());
            }
            catch (Exception ex)
            {
                Debug.WriteLine(string.Format("MessageReceived-Error: Failed to convert first value (Engine) of message to integer - {0}.", ex.Message));
                return;
            }

            switch (engineType)
            {
                case (int)EngineIOMessageType.Connect:
                    // Connect
                    Debug.WriteLine("Connect Received.");
                    break;
                case (int)EngineIOMessageType.Close:
                    // Close
                    Debug.WriteLine("Close Received.");
                    break;
                case (int)EngineIOMessageType.Ping:
                    // Ping
                    Debug.WriteLine("Ping Received.");
                    this.SendPong();
                    break;
                case (int)EngineIOMessageType.Pong:
                    // Pong
                    Debug.WriteLine("Pong Received.");
                    break;
                case (int)EngineIOMessageType.Message:
                    // Message
                    Debug.WriteLine("Message Received.");
                    this.HandleMessage(message);
                    break;
                case (int)EngineIOMessageType.Upgrade:
                    // Upgrade
                    Debug.WriteLine("Upgrade Received.");
                    break;
                case (int)EngineIOMessageType.Noop:
                    // Noop
                    Debug.WriteLine("Noop Received.");
                    this.UpgradeToWebsocket();
                    break;
                default:
                    Debug.WriteLine("Error: Invalid message received.");
                    Debug.WriteLine("Message: {0}", message);
                    break;
            }
        }

        /// <summary>
        /// Manages the socket.IO message. To reach this method, the message provided should have already been managed in 'Message Received'.
        /// Example messages: 4#
        ///                 : 42['event','data'
        /// </summary>
        /// <param name="message">The message.</param>
        protected void HandleMessage(string message)
        {
            int socketType;

            try
            {
                socketType = int.Parse(message[1].ToString());
            }
            catch (Exception ex)
            {
                Debug.WriteLine(string.Format("MessageReceived-Error: Failed to convert second value (Socket) of message to integer - {0}.", ex.Message));
                return;
            }

            switch (socketType)
            {
                case (int)SocketIOMessageType.Connect:
                    Debug.WriteLine("Connect Message Received");
                    // SocketDisconnected()
                    break;
                case (int)SocketIOMessageType.Disconnect:
                    Debug.WriteLine("Disconnect Message Received");
                    // SocketConnected();
                    break;
                case (int)SocketIOMessageType.Event:
                    Debug.WriteLine("Event Message Received");
                    this.HandleEvent(message);
                    break;
                case (int)SocketIOMessageType.Ack:
                    Debug.WriteLine("Ack Message Received");
                    // ToDo: this
                    break;
                case (int)SocketIOMessageType.Error:
                    Debug.WriteLine("Error Message Received");
                    // SocketErrorReceived();
                    break;
                case (int)SocketIOMessageType.Binary_Event:
                    Debug.WriteLine("Binary_Event Message Received");
                    // ToDo: this
                    break;
                case (int)SocketIOMessageType.Binary_Ack:
                    Debug.WriteLine("Binary_Ack Message Received");
                    // ToDo: this
                    break;
                default:
                    Debug.WriteLine("SocketType-Error: There was no socket type provided in the Message.");
                    break;
            }
        }

        /// <summary>
        /// Handle a message event type.
        /// Example Message: 42["'eventName'","{data}"]
        /// </summary>
        /// <param name="message">The message.</param>
        protected void HandleEvent(string message)
        {
            int indexOfArrayOpen = message.IndexOf('[');
            string array = message.Substring(indexOfArrayOpen + 1, message.IndexOf(']') - indexOfArrayOpen - 1);
            string[] components = array.Split(',');

            string eventName = components[0].Trim('\"');
            JObject jsonObjData = null;
            string jsonstring = string.Empty;

            // Check if the message has data with it.
            // Note that the data should be JSON serializable. 
            if (components.Length > 1)
            {
                // Check if the opening message contains a namespace/endpoint.
                // if so, work around the comma that comes with it.
                // (eg - 42/namespace,[eventname,data]
                int messageCommaStartIndex = 0;

                if (message.Substring(0, indexOfArrayOpen).Contains("/"))
                {
                    messageCommaStartIndex = message.IndexOf(",") + 1;
                }

                jsonstring = message.Substring(message.IndexOf(',', messageCommaStartIndex) + 1, message.IndexOf(']') - message.IndexOf(',', messageCommaStartIndex) - 1);

                try
                {
                    jsonObjData = JObject.Parse(jsonstring.Trim('\"'));
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Error while parsing data into Json format: " + ex.Message);
                    return;
                }
            }

            if (this.eventHandlers.ContainsKey(eventName))
            {
                var handlers = this.eventHandlers[eventName];
                foreach (var handler in handlers)
                {
                    Debug.WriteLine("Running Event: {0} with data: {1}", eventName, jsonstring);
                    handler(jsonObjData);
                }
            }
        }

        /// <summary>
        /// Used when SocketIO is Long Polling.
        /// Sends an Http Post to the server.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns>Returns <c>true</c> if the Http Post returns a success code; otherwise, <c>false</c>.</returns>
        protected async Task<bool> SendHttpPost(string message)
        {
            string scheme = this.IsSecure ? "https" : "http";
            string cmdUrl = string.Format("{0}://{1}:{2}/{3}/{4}&t={5}-{6}&sid={7}", scheme, this.Host, this.Port, SocketIOUrlString, SessionQuery, this.MessageTimestamp, this.MessageAckId, this.sessionID);

            HttpClientHandler clientHandler = new HttpClientHandler();
            System.Net.Cookie cookie = new System.Net.Cookie();
            cookie.Domain = this.Host;
            cookie.Name = "io";
            cookie.Value = this.sessionID;
            clientHandler.CookieContainer.Add(new Uri(cmdUrl), cookie);

            using (HttpClient webClient = new HttpClient(clientHandler))
            {
                string jsonTextRequestBody = string.Format("{0}:{1}", message.Length, message);
                webClient.BaseAddress = new Uri(cmdUrl);
                webClient.DefaultRequestHeaders.Accept.Clear();
                HttpContent content = new StringContent(jsonTextRequestBody, Encoding.UTF8, "application/octet-stream");

                using (HttpResponseMessage response = await webClient.PostAsync(cmdUrl, content))
                {
                    return response.IsSuccessStatusCode;
                }
            }
        }

        /// <summary>
        /// Used when SocketIO is Long Polling.
        /// Sends the get request to the server which will return any messages the server has for this client.
        /// </summary>
        /// <returns>Returns <c>true</c> if the Http Get is successful; otherwise, <c>false</c>.</returns>
        protected async Task<bool> SendHttpGet()
        {
            string scheme = this.IsSecure ? "https" : "http";
            string cmdUrl = string.Format("{0}://{1}:{2}/{3}/{4}&t={5}-{6}&sid={7}", scheme, this.Host, this.Port, SocketIOUrlString, SessionQuery, this.MessageTimestamp, this.MessageAckId, this.sessionID);

            using (HttpClient webClient = new HttpClient())
            {
                webClient.BaseAddress = new Uri(cmdUrl);
                webClient.DefaultRequestHeaders.Accept.Clear();
                try
                {
                    using (HttpResponseMessage response = await webClient.GetAsync(cmdUrl))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            using (HttpContent content = response.Content)
                            {
                                string responseText = await content.ReadAsStringAsync();
                                this.ParseMessage(responseText);

                                // If the connection is longpolling, as soon as a GET request is returned, the client should
                                // send a new one right after. 
                                if (this.SocketConnectionType == ConnectionType.LongPolling)
                                {
                                    // Do not await
                                    this.SendHttpGet();
                                }
                            }
                        }
                        else
                        {
                            this.Status = ConnectionStatus.Closed;
                            this.AttemptReconnect();
                        }

                        return response.IsSuccessStatusCode;
                    }
                }
                catch (TaskCanceledException ex)
                {
                    Debug.WriteLine("Error: A GET Request has been canceled. This will force the socket.io server to kill the session. Reconnecting is required!");
                    Debug.WriteLine(ex.Message);
                    return false;
                }
            }
        }

        /// <summary>
        /// Called when a message is received.
        /// </summary>
        /// <param name="message">The message.</param>
        protected abstract void MessageReceived(string message);

        /// <summary>
        /// Send the message to disconnect from the specified endpoint.
        /// If the endpoint is not given, this will call for the socket connection to disconnect. 
        /// </summary>
        /// <param name="o">The sender?</param>
        /// <param name="socketNamespace">The socket namespace.</param>
        protected virtual void SendDisconnectMessage(object o, string socketNamespace = "")
        {
            if (this.Status == ConnectionStatus.Connected)
            {
                Debug.WriteLine("Sending Disconnect Message.");
                this.Send(new Disconnect(socketNamespace));

                // If the Endpoint is blank, disconnect the socket connection.
                if (string.IsNullOrEmpty(socketNamespace))
                {
                    this.HeartbeatTimer.Change(0, 0);
                    this.HeartbeatTimer.Dispose();
                    this.Status = ConnectionStatus.Closed;
                }
            }
            else
            {
                Debug.WriteLine("Error: websocket must be 'Connected' in order to disconnect");
            }
        }

        /// <summary>
        /// Sends the ping / heartbeat to keep the session alive.
        /// </summary>
        protected void SendPing()
        {
            if (this.Status == ConnectionStatus.Connected)
            {
                this.Send(new Ping());
            }
            else
            {
                this.HeartbeatTimer.Change(0, 0);
            }
        }

        /// <summary>
        /// Sends the pong.
        /// </summary>
        protected void SendPong()
        {
            Debug.WriteLine("Sending Pong.");
            if (this.Status == ConnectionStatus.Connected)
            {
                this.Send(new Pong());
            }
            else
            {
                Debug.WriteLine("Error: trying to send Pong but not connected.");
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Used when SocketIO is Long Polling.
        /// Parses one or more messages that the server has sent to the client. Strips out the individual messages and passes them
        /// to the client message received event handler.
        /// </summary>
        /// <param name="message">The message.</param>
        private void ParseMessage(string message)
        {
            // Messages will come in the format: <length>:<message>
            // eg. 1:2 
            // eg. 6:2hello
            // They can be batched together: 1:26:2hello
            string singleMessageLength = string.Empty;
            int colonIndex = 1;

            for (int i = 0; i < message.Length; i++)
            {
                if (message[i] != ':')
                {
                    singleMessageLength += message[i];
                    continue;
                }

                if (singleMessageLength != string.Empty)
                {
                    try
                    {
                        int length = Convert.ToInt32(singleMessageLength);
                        i += length;
                        string singleMessage = message.Substring(message.IndexOf(':', colonIndex) + 1, length);
                        // if there is more than one message, ignore the ones that have already been processed.
                        colonIndex = message.IndexOf(':', length);
                        singleMessageLength = string.Empty;
                        this.MessageReceived(singleMessage);
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(string.Format("Error: Failed to parse polling message: {0}", ex.Message));
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// Will start Long-Polling if it is not already.
        /// </summary>
        private async void StartPolling()
        {
            if (!this.pollingInProgress)
            {
                this.pollingInProgress = true;

                // Do not await
                this.SendHttpGet();
            }
        }

        #endregion
    }
}
