﻿//------------------------------------------------------------------
//
// Created by: Daniel Deak
// 
//------------------------------------------------------------------

namespace SocketIO.Client.CommonPortable
{
    #region Using Statements

    using System;
    using System.Collections.Generic;

    using Newtonsoft.Json;

    #endregion

    /// <summary>
    /// Class representing a Socket IO Connection String.
    /// </summary>
    public class SocketConnectionString
    {
        #region Private Members

        #endregion

        #region Constructors

        #endregion

        #region Events

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the Socket Id identifier.
        /// </summary>
        /// <value>
        /// The Socket Id identifier.
        /// </value>
        [JsonProperty("sid")]
        public string SId { get; set; }

        /// <summary>
        /// Gets or sets the upgrades.
        /// </summary>
        /// <value>
        /// The upgrades.
        /// </value>
        [JsonProperty("upgrades")]
        public List<string> Upgrades { get; set; }

        /// <summary>
        /// Gets or sets the ping interval.
        /// </summary>
        /// <value>
        /// The ping interval.
        /// </value>
        [JsonProperty("pingInterval")]
        public int PingInterval { get; set; }

        /// <summary>
        /// Gets or sets the ping timeout.
        /// </summary>
        /// <value>
        /// The ping timeout.
        /// </value>
        [JsonProperty("pingTimeout")]
        public int PingTimeout { get; set; }

        #endregion

        #region Public Methods

        #endregion

        #region Protected Methods

        #endregion
    }
}
