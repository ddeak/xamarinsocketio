﻿//------------------------------------------------------------------
//
// Created by: Daniel Deak
// 
//------------------------------------------------------------------

namespace SocketIO.Client.CommonPortable
{
    #region Using Statements

    using System;

    #endregion

    /// <summary>
    /// Enumeration representing the type of a connection, e.g. a WebSocket Connection.
    /// </summary>
    public enum ConnectionType
    {
        /// <summary>
        /// The connection uses web sockets.
        /// </summary>
        WebSocket,

        /// <summary>
        /// The connection uses long polling.
        /// </summary>
        LongPolling
    }
}
