﻿//------------------------------------------------------------------
//
// Created by: Daniel Deak
// 
//------------------------------------------------------------------

namespace SocketIO.Client.CommonPortable.Message
{
    #region Using Statements

    using System;
    using System.Collections;

    #endregion

    /// <summary>
    /// The Upgrade Message class.
    /// </summary>
    public class Upgrade : Message
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Upgrade"/> class.
        /// </summary>
        /// <param name="endpoint">The endpoint.</param>
        public Upgrade(string endpoint = "") : base(endpoint)
        {
            this.EngineMessageType = (int)EngineIOMessageType.Upgrade;
        }
    }
}
