﻿//------------------------------------------------------------------
//
// Created by: Daniel Deak
// 
//------------------------------------------------------------------

namespace SocketIO.Client.CommonPortable.Message
{
    #region Using Statements

    using System;

    #endregion

    /// <summary>
    /// Provides an interface for Messages.
    /// </summary>
    public interface IMessage
    {
        #region Events

        #endregion

        #region Properties

        /// <summary>
        /// Gets the encoded message text.
        /// </summary>
        /// <value>
        /// The encoded message text.
        /// </value>
        string Encoded { get; }

        /// <summary>
        /// Gets or sets the end point.
        /// </summary>
        /// <value>
        /// The end point.
        /// </value>
        string EndPoint { get; set; }

        /// <summary>
        /// Gets or sets the type of the Engine IO message.
        /// </summary>
        /// <value>
        /// The type of the Engine IO message.
        /// </value>
        int EngineMessageType { get; set; }

        /// <summary>
        /// Gets or sets the raw message.
        /// </summary>
        /// <value>
        /// The raw message.
        /// </value>
        string RawMessage { get; set; }

        /// <summary>
        /// Gets or sets the type of the Socket IO message.
        /// </summary>
        /// <value>
        /// The type of the Socket IO message.
        /// </value>
        int? SocketMessageType { get; set; }

        #endregion

        #region Methods

        #endregion
    }
}
