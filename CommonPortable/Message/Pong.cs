﻿//------------------------------------------------------------------
//
// Created by: Daniel Deak
// 
//------------------------------------------------------------------

namespace SocketIO.Client.CommonPortable.Message
{
    #region Using Statements

    using System;

    #endregion

    /// <summary>
    /// The pong / heartbeat echo message.
    /// </summary>
    public class Pong : Message
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Pong"/> class.
        /// </summary>
        public Pong()
        {
            this.EngineMessageType = (int)EngineIOMessageType.Pong;
        }
    }
}
