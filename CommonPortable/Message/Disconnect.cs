﻿//------------------------------------------------------------------
//
// Created by: Daniel Deak
// 
//------------------------------------------------------------------

namespace SocketIO.Client.CommonPortable.Message
{
    #region Using Statements

    using System;

    #endregion

    /// <summary>
    /// The disconnect message class.
    /// </summary>
    public class Disconnect : Message
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Disconnect"/> class.
        /// </summary>
        /// <param name="endpoint">The endpoint.</param>
        public Disconnect(string endpoint = "") : base(endpoint)
        {
            this.EngineMessageType = (int)EngineIOMessageType.Message;
            this.SocketMessageType = (int)SocketIOMessageType.Disconnect;          
        }
    }
}
