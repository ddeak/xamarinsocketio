﻿//------------------------------------------------------------------
//
// Created by: Daniel Deak
// 
//------------------------------------------------------------------

namespace SocketIO.Client.CommonPortable.Message
{
    #region Using Statements

    using System;

    #endregion

    /// <summary>
    /// Abstract base class for Messages.
    /// </summary>
    public abstract class Message : IMessage
    {
        #region Private Members

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Message"/> class.
        /// </summary>
        /// <param name="endpoint">The endpoint.</param>
        public Message(string endpoint = "")
        {
            this.EndPoint = endpoint;
        }

        #endregion

        #region Events

        #endregion

        #region Properties

        /// <summary>
        /// Gets the encoded message text.
        /// </summary>
        /// <value>
        /// The encoded message text.
        /// </value>
        public virtual string Encoded
        {
            get
            {
                return string.Format(
                    "{0}{1}{2}", 
                    this.EngineMessageType, 
                    this.SocketMessageType == null ? string.Empty : this.SocketMessageType.ToString(),
                    string.IsNullOrEmpty(this.EndPoint) ? string.Empty : ("this./" + this.EndPoint + ","));
            }
        }

        /// <summary>
        /// Gets or sets the end point.
        /// </summary>
        /// <value>
        /// The end point.
        /// </value>
        public string EndPoint { get; set; }

        /// <summary>
        /// Gets or sets the type of the Engine IO message.
        /// </summary>
        /// <value>
        /// The type of the Engine IO message.
        /// </value>
        public int EngineMessageType { get; set; }

        /// <summary>
        /// Gets or sets the type of the Socket IO message.
        /// </summary>
        /// <value>
        /// The type of the Socket IO message.
        /// </value>
        public int? SocketMessageType { get; set; }

        /// <summary>
        /// Gets or sets the raw message.
        /// </summary>
        /// <value>
        /// The raw message.
        /// </value>
        public string RawMessage { get; set; }

        #endregion

        #region Public Methods

        #endregion

        #region Protected Methods

        #endregion
    }
}
