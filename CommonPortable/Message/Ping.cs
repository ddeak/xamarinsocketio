﻿//------------------------------------------------------------------
//
// Created by: Daniel Deak
// 
//------------------------------------------------------------------

namespace SocketIO.Client.CommonPortable.Message
{
    #region Using Statements

    using System;

    #endregion

    /// <summary>
    /// The ping / heartbeat message.
    /// </summary>
    public class Ping : Message
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Ping"/> class.
        /// </summary>
        public Ping()
        {
            this.EngineMessageType = (int)EngineIOMessageType.Ping;
        }
    }
}
