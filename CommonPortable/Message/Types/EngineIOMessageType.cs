﻿//------------------------------------------------------------------
//
// Created by: Daniel Deak
// 
//------------------------------------------------------------------

namespace SocketIO.Client.CommonPortable.Message
{
    #region Using Statements

    using System;

    #endregion

    /// <summary>
    /// Enumeration representing the type of an Engine IO Message, e.g. Ping, Pong, etc.
    /// </summary>
    public enum EngineIOMessageType
    {
        /// <summary>
        /// The connect message.
        /// </summary>
        Connect = 0,

        /// <summary>
        /// The close message.
        /// </summary>
        Close = 1,

        /// <summary>
        /// The ping message, sent from clients to servers as a heartbeat to keep the session alive.
        /// </summary>
        Ping = 2,

        /// <summary>
        /// The pong message, sent from the server to the client in response to a ping event.
        /// </summary>
        Pong = 3,

        /// <summary>
        /// The message.
        /// </summary>
        Message = 4,

        /// <summary>
        /// The upgrade message - sent by a client to request upgrade from Long Polling to WebSockets.
        /// </summary>
        Upgrade = 5,

        /// <summary>
        /// The no operation message.
        /// </summary>
        Noop = 6
    }
}
