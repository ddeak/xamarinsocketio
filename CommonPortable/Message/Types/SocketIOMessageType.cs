﻿//------------------------------------------------------------------
//
// Created by: Daniel Deak
// 
//------------------------------------------------------------------

namespace SocketIO.Client.CommonPortable
{
    #region Using Statements

    using System;

    #endregion

    /// <summary>
    /// Enumeration representing the type of a Socket IO Message, e.g. Event, Error, etc.
    /// </summary>
    public enum SocketIOMessageType
    {
        /// <summary>
        /// The connect message.
        /// </summary>
        Connect = 0,

        /// <summary>
        /// The disconnect message.
        /// </summary>
        Disconnect = 1,

        /// <summary>
        /// The event message.
        /// </summary>
        Event = 2,

        /// <summary>
        /// The acknowledgement message.
        /// </summary>
        Ack = 3,

        /// <summary>
        /// The error message.
        /// </summary>
        Error = 4,

        /// <summary>
        /// The binary event message.
        /// </summary>
        Binary_Event = 5,

        /// <summary>
        /// The binary acknowledgement message.
        /// </summary>
        Binary_Ack = 6
    }
}
