﻿//------------------------------------------------------------------
//
// Created by: Daniel Deak
// 
//------------------------------------------------------------------

namespace SocketIO.Client.CommonPortable.Message
{
    #region Using Statements

    using System;
    using System.Collections;

    #endregion

    /// <summary>
    /// The Event Message class.
    /// </summary>
    public class EventMessage : Message
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="EventMessage"/> class.
        /// </summary>
        /// <param name="eventName">Name of the event.</param>
        /// <param name="data">The data.</param>
        /// <param name="endpoint">The endpoint.</param>
        public EventMessage(string eventName, IEnumerable data, string endpoint = "") : base(endpoint)
        {
            this.EngineMessageType = (int)EngineIOMessageType.Message;
            this.SocketMessageType = (int)SocketIOMessageType.Event;
            this.EventName = eventName;
            this.Data = data;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        /// <value>
        /// The data.
        /// </value>
        public IEnumerable Data { get; set; }

        /// <summary>
        /// Gets the encoded message value.
        /// </summary>
        /// <value>
        /// The encoded message.
        /// </value>
        public override string Encoded
        {
            get
            {
                return string.Format(
                    "{0}{1}[\"{2}\",{3}]",
                    this.EngineMessageType,
                    this.SocketMessageType,
                    this.EventName,
                    this.Data);
            }
        }

        /// <summary>
        /// Gets or sets the name of the event.
        /// </summary>
        /// <value>
        /// The name of the event.
        /// </value>
        public string EventName { get; set; }

        /// <summary>
        /// Gets or sets the message identifier.
        /// </summary>
        /// <value>
        /// The message identifier.
        /// </value>
        public string MessageID { get; set; }

        #endregion
    }
}
