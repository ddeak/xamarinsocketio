﻿//------------------------------------------------------------------
//
// Created by: Daniel Deak
// 
//------------------------------------------------------------------

namespace SocketIO.Client.CommonPortable.Message
{
    #region Using Statements

    using System;

    #endregion

    /// <summary>
    /// The connect message class.
    /// </summary>
    public class Connect : Message
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Connect"/> class.
        /// </summary>
        /// <param name="endpoint">The endpoint.</param>
        public Connect(string endpoint = "") : base(endpoint)
        {
            this.EngineMessageType = (int)EngineIOMessageType.Message;
            this.SocketMessageType = (int)SocketIOMessageType.Connect;            
        }
    }
}
