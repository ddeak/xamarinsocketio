﻿//------------------------------------------------------------------
//
// Created by: Daniel Deak
// 
//------------------------------------------------------------------

namespace SocketIO.Client.CommonPortable
{
    #region Using Statements

    using System;
    using System.Threading.Tasks;

    using SocketIO.Client.CommonPortable.Message;
    using Newtonsoft.Json.Linq;

    #endregion

    /// <summary>
    /// The interface definition for a Socket IO Client.
    /// </summary>
    public interface ISocketClient
    {
        #region Events

        /// <summary>
        /// Occurs when a session is established, regardless of connection type.
        /// </summary>
        event Action<IMessage> SessionEstablished;

        /// <summary>
        /// Occurs when a socket fails to connect.
        /// </summary>
        event Action<string> SocketFailedToConnect;

        /// <summary>
        /// Occurs when a session is disconnected.
        /// </summary>
        event Action<string> SessionDisconnected;

        /// <summary>
        /// Occurs when timed out.
        /// </summary>
        event Action TimedOut;

        #endregion

        #region Properties

        #endregion

        #region Methods

        #endregion

        /// <summary>
        /// Connects to a Socket IO server asynchronously.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <returns>Task object for feedback.</returns>
        Task ConnectAsync(string query);

        /// <summary>
        /// Sends a message.
        /// </summary>
        /// <param name="message">The message to send.</param>
        void Send(IMessage message);

        /// <summary>
        /// Subscribes the client to notifications of specific events.
        /// </summary>
        /// <param name="name">The name of the event to subscribe to.</param>
        /// <param name="handler">The handler to call when a message with the named event occurs.</param>
        void On(string name, Action<JToken> handler);
    }
}
