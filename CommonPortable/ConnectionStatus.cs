﻿//------------------------------------------------------------------
//
// Created by: Daniel Deak
// 
//------------------------------------------------------------------

namespace SocketIO.Client.CommonPortable
{
    #region Using Statements

    using System;

    #endregion

    /// <summary>
    /// Enumeration representing the status of a connection.
    /// </summary>
    public enum ConnectionStatus
    {
        /// <summary>
        /// The connection is connected.
        /// </summary>
        Connected,

        /// <summary>
        /// The connection is closed.
        /// </summary>
        Closed,

        /// <summary>
        /// The connection is connecting.
        /// </summary>
        Connecting
    }
}
